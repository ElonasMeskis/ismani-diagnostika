import Login from '../components/login/Login';
import React from 'react';

const LoginPage: React.FunctionComponent = (): React.ReactElement => {
  return (
    <Login />
  );
};

export default LoginPage;
