import React from 'react';
import App from '../components/App';
import { shallow } from 'enzyme';
import '../configs/tests.config';

describe('tests <App>', () => {

  it('should render App', () => {
    const wrapper = createWidget();
    expect(wrapper.find('.App')).toHaveLength(1);
  });

  const createWidget = (): any => shallow(<App />);
});
