import './styles/main.scss';

import AppRouter from './routes/Router';
import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { cyan } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: cyan,
  },
});

const Root = (): React.ReactElement => (
  <ThemeProvider theme={theme}>
    <AppRouter />
  </ThemeProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
