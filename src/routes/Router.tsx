import { Route, Router, Switch } from 'react-router-dom';

import Home from '../components/home/Home';
import LoginPage from '../pages/LoginPage';
import React from 'react';
import { history } from '../configs/history.config';

const AppRouter: React.FunctionComponent = (): React.ReactElement => {
  return (
    <Router history={history}>
      <Switch>
        <Route path='/' exact component={LoginPage} />
        <Route path='/home' exact component={Home} />
        <Route component={Home} />
      </Switch>
    </Router>
  );
};

export default AppRouter;
