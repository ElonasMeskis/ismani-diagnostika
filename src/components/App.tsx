import React, { Component } from 'react';

import { Button } from '@material-ui/core';

class App extends Component {
  render(): React.ReactElement {
    return (
      <div className='App'>
        <Button variant='contained' color='primary'>
          Hello World
        </Button>
      </div>
    );
  }
}

export default App;
