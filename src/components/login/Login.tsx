import React, { useState } from 'react';

import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { history } from '../../configs/history.config';
import { makeStyles } from '@material-ui/core/styles';

const Copyright = (): React.ReactElement => {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'Visos teisės saugomos © '}
      <Link color='inherit' href='http://localhost:3000'>
        Išmani diagnostika
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
};

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default (): React.ReactElement => {
  const classes = useStyles();
  const [email, setEmail]: any = useState('');
  const [password, setPassword]: any = useState('');

  const loginLocal = (e: any) => {
    e.preventDefault();
    history.push('/diagnosis');
  };

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <div className='logo' />
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Prisijungimas
        </Typography>
        <form className={classes.form} noValidate onSubmit={loginLocal}>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            id='email'
            label='El. paštas'
            name='email'
            autoComplete='email'
            autoFocus
            onChange={e => setEmail(e.target.value)}
            value={email}
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            name='password'
            label='Slaptažodis'
            type='password'
            id='password'
            autoComplete='current-password'
            onChange={e => setPassword(e.target.value)}
            value={password}
          />
          <FormControlLabel
            control={<Checkbox value='remember' color='primary' />}
            label='Prisiminti'
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
          >
            Prisijungti
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href='#' variant='body2'>
                Pamiršote slaptažodį?
              </Link>
            </Grid>
            <Grid item>
              <Link href='#' variant='body2'>
                {`Registracija`}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
};
