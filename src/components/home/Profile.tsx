import { Button, Snackbar, TextField } from '@material-ui/core';
import React, { SyntheticEvent, useState } from 'react';

import { Alert } from './Alert';

export const Profile = () => {
  const [user, setUser]: any = useState(undefined);
  const [fields, setFields]: any = useState({});
  const [open, setOpen]: any = React.useState(false);

  const handleAlert = () => {
    setOpen(true);
  };

  const handleClose = (event?: SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const setValue = (key: string, value: any) => {
    if (!value.length) {
      delete fields[key];
      return;
    }
    fields[key] = value;
  };

  const saveUser = async () => {
    handleAlert();
  };

  return (
    <div className='page profile'>
      <h2>Profilis</h2>
      <form className='profile-form'>
        <TextField
          error={false}
          label='Firstname'
          defaultValue='Ferdinandas'
          onChange={e => setValue('firstname', e.target.value)}
        />
        <TextField
          error={false}
          label='Lastname'
          defaultValue='Magelanas'
          onChange={e => setValue('lastname', e.target.value)}
        />
        <TextField
          error={false}
          label='Email'
          defaultValue='magelanas@hotmail.com'
          disabled={true}
        />
        <TextField
          error={false}
          label='New Password'
          onChange={e => setValue('password', e.target.value)}
          type='password'
        />
        <Button variant='outlined' color='primary' onClick={saveUser}>Save</Button>
      </form>

      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          variant='success'
          message='Profilis buvo sėkmingai atnaujintas'
        />
      </Snackbar>
    </div>
  );
};