import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Paper, Snackbar, Theme, Typography, createStyles, makeStyles } from '@material-ui/core'
import React, { SyntheticEvent, useState } from 'react'

import { Alert } from './Alert';
import Avatar from '@material-ui/core/Avatar';
import { history } from '../../configs/history.config';

const doctors = [
  { name: 'Genadijus Antetokounmpo', avatar: 'https://www.proballers.com/ul/player/46975-1-5d6e9512964a34.jpg' },
  { name: 'Džonas Valančiūnas', avatar: 'https://a.espncdn.com/i/headshots/nba/players/full/6477.png' },
  { name: 'Gitanas Nausėda', avatar: 'https://www.vle.lt/Portals/0/Images/Article/7657-2.jpg' },
  { name: 'Mantas Katleris', avatar: 'https://upload.wikimedia.org/wikipedia/commons/5/55/Mantas_Katleris.jpg' },
  { name: 'Dziundze Pikmonkčang', avatar: 'http://www.vartiklis.lt/history/russian/siberia/buryat.jpg' },
  { name: 'Haris Narkoteris', avatar: 'https://vignette.wikia.nocookie.net/harrypotter/images/8/8d/PromoHP7_Harry_Potter.jpg/revision/latest?cb=20140603201724' },
]

export const Registration = () => {
  const [open, setOpen] = useState(false);
  const [selectedDoctor, setSelectedDoctor]: any = useState({ name: '' });
  const [alert, setAlert]: any = React.useState(false);

  const handleAlert = () => {
    setAlert(true);
    setOpen(false);
  };

  const handleAlertClose = (event?: SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setAlert(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSelectedDoctor = (doctor: any) => {
    setSelectedDoctor(doctor);
    handleClickOpen();
  };

  const register = () => {
    handleAlert();
  }

  return (
    <div className='page'>
      <h2>Registracija</h2>
      <h4>Pasirinkite gydytoją pas kurį norite registruotis vizitui</h4>
      <div className='doctors-list'>
        {doctors.map(doctor => (
          <Paper className='doctor' onClick={() => handleSelectedDoctor(doctor)}>
            <Typography component="h2">
              {doctor.name}
            </Typography>
            <Typography variant="h5" component="h3">
              <Avatar alt="Remy Sharp" src={doctor.avatar} />
            </Typography>
          </Paper>
        ))}
      </div>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Ar tikrai norite registruotis pas {selectedDoctor.name}</DialogTitle>

        <DialogContent>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Atšaukti
          </Button>
          <Button onClick={register} color="primary">
            Registruoti
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar
        open={alert}
        autoHideDuration={6000}
        onClose={handleAlertClose}
      >
        <Alert
          onClose={handleAlertClose}
          variant='success'
          message={`Buvai sėkmingai užregistruotas pas ${selectedDoctor.name}`}
        />
      </Snackbar>
    </div>
  )
}
