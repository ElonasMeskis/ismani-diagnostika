import { Divider, ListSubheader } from '@material-ui/core';

import BasketballIcon from '@material-ui/icons/YoutubeSearchedFor';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MatchesIcon from '@material-ui/icons/ThumbsUpDown';
import ProfileIcon from '@material-ui/icons/AccountBox';
import React from 'react';
import SoccerIcon from '@material-ui/icons/Event';
import StakesIcon from '@material-ui/icons/PostAdd';
import UsersIcon from '@material-ui/icons/SupervisedUserCircle';
import { history } from '../../configs/history.config';

export const Menu = () => {
  return (
    <div>
      <ListSubheader inset>Navigacija</ListSubheader>
      <Divider />
      <ListItem button onClick={() => history.push('/diagnosis')}>
        <ListItemIcon>
          <BasketballIcon />
        </ListItemIcon>
        <ListItemText primary='Ligos diagnozė' />
      </ListItem>
      <ListItem button onClick={() => history.push('/registration')}>
        <ListItemIcon>
          <SoccerIcon />
        </ListItemIcon>
        <ListItemText primary='Registracija' />
      </ListItem>
      <Divider />
      <ListSubheader inset>Vartotojas</ListSubheader>
      <Divider />
      <ListItem button onClick={() => history.push('/my-registrations')}>
        <ListItemIcon>
          <StakesIcon />
        </ListItemIcon>
        <ListItemText primary='Mano registracijos' />
      </ListItem>
      <ListItem button onClick={() => history.push('/profile')}>
        <ListItemIcon>
          <ProfileIcon />
        </ListItemIcon>
        <ListItemText primary='Profilis' />
      </ListItem>
    </div>
  );
};
