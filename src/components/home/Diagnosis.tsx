import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slider, Typography } from '@material-ui/core';
import React, { useState } from 'react'
import { Theme, createStyles, makeStyles, useTheme } from '@material-ui/core/styles';

import Chip from '@material-ui/core/Chip';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { history } from '../../configs/history.config';

const bodyParts = [
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'kairysis-petys', description: 'Galva' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'petys', description: 'Petys' },
  { part: 'kaklas', description: 'Kaklas' },
  { part: 'petys', description: 'Petys' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'ranka', description: 'Ranka' },
  { part: 'krutine', description: 'Krūtinė' },
  { part: 'ranka', description: 'Ranka' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'pilvas', description: 'Pilvas' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'delnas', description: 'Delnas' },
  { part: '', description: '' },
  { part: 'organas', description: 'Tarpukojis' },
  { part: '', description: '' },
  { part: 'delnas', description: 'Delnas' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'koja', description: 'Koja' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: 'slaunis', description: 'Šlaunis' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
  { part: '', description: '' },
];

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }),
);

const names = [
  'Skausmas',
  'Dirginimas',
  'Jautrumas',
  'Niežėjimas',
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name: string, personName: string[], theme: Theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export const Diagnosis = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [selectedPart, setSelectedPart]: any = useState({ index: -1, part: '', description: '' })
  const [personName, setPersonName] = useState<string[]>([]);
  const [open, setOpen] = useState(false);
  const [diagnosis, setDiagnosis]: any = useState(undefined);
  const [diagnosisDescription, setDiagnosisDescription]: any = useState(undefined);

  const handleClickOpen = () => {
    setOpen(true);
    getDiagnosis();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setPersonName(event.target.value as string[]);
  };

  const handleSelectedPart = (item: any) => {
    setSelectedPart(item);
  }

  const getDiagnosis = () => {
    setDiagnosis(undefined);
    setDiagnosisDescription(undefined);
    setTimeout(() => {
      setDiagnosis([
        { liga: 'Sloga', description: 'Varva nosis, reikia servetėlių ir dažnai pūsti nosį' },
        { liga: 'Peršalimas', description: 'Visada šalta, reikia šilčiau rengtis' },
        { liga: 'Vėžys', description: 'Nieko gero, bet nenuleisk rankų' },
        { liga: 'Saulės smūgis', description: 'Gavai perdaug ultra-violetinių spindulių nuo saulės, turbūt per ilgai buvai ant saulės' },
      ]);
    }, 4000);

  }

  console.log(selectedPart);
  return (
    <div className='page'>
      <h2>Ligos diagnozė</h2>

      <h4>Pasirinkite kūno vietą, kuria skundžiatės.</h4>
      <div className='human-body'>
        <div className='prototype'>
          {bodyParts.map((item, index) => (
            <div
              key={index}
              className={`body-part ${selectedPart.index === index ? 'selected' : ''} ${item.part !== '' ? 'selectable' : ''}`}
              onClick={() => handleSelectedPart({ index, ...item })}
            >
              {item.description}
            </div>
          ))}
        </div>
      </div>
      <div className='symptoms'>
        <h4>Pasirinkta: {selectedPart.description}</h4>
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-mutiple-chip-label">Simptomai</InputLabel>
          <Select
            labelId="demo-mutiple-chip-label"
            id="demo-mutiple-chip"
            multiple
            value={personName}
            onChange={handleChange}
            input={<Input id="select-multiple-chip" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {(selected as string[]).map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {names.map(name => (
              <MenuItem key={name} value={name} style={getStyles(name, personName, theme)}>
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <div className='slider'>
          <Typography id="discrete-slider" gutterBottom>
            Simptomų intensyvumas (%)
          </Typography>
          <Slider
            defaultValue={30}
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            step={10}
            marks
            min={10}
            max={100}
          />
        </div>
        <Button variant='contained' disabled={selectedPart.index === -1} color='primary' onClick={handleClickOpen}>Analizuoti</Button>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Ligos diagnozė</DialogTitle>

        <DialogContent>
          {diagnosis ? (
            <div>
              <DialogContentText id="alert-dialog-description">
                Sveikiname, ligos diagnozė sėkminga! Galimų ligų sąrašas:
              </DialogContentText>
              {diagnosis.map((diag: any, index: number) => (
                <div className='liga'>
                  <DialogContentText id="alert-dialog-description">
                    {index + 1}. {diag.liga}
                  </DialogContentText>
                  <DialogContentText id="alert-dialog-description">
                    {diag.description}
                  </DialogContentText>
                </div>
              ))}
            </div>
          ) : (
              <DialogContentText>
                <div className='loading'>
                  <CircularProgress />
                </div>
              </DialogContentText>
            )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Atšaukti
          </Button>
          <Button onClick={() => history.push('/registration')} color="primary">
            Registracija gydymui
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}